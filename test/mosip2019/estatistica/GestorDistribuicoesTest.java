/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.estatistica;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class GestorDistribuicoesTest {
    
    public GestorDistribuicoesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of gerarTempoExponencialSegundosEntreProximoCliente method, of class GestorDistribuicoes.
     */
    @Test
    public void testgerarValorDistribuicaoExponencial() {
        System.out.println("gerarTempoExponencialSegundosEntreProximoCliente");
        GestorDistribuicoes instance = new GestorDistribuicoes(1000000);
        
        double media = 16.43026;   
        double tempoMedio = 0;
        int tentativas = 1000;
        for(int i = 0; i< tentativas;i++){
            double t = instance.gerarValorDistribuicaoExponencial(media);
            tempoMedio += t;
            System.out.println("" + (long) t);
            
        }
        tempoMedio /= tentativas;
        System.out.println("Erro: " + (media - tempoMedio));
        
        assertEquals(media, tempoMedio, 5);        
    }    
    
    /**
     * Test of valorCaixa method, of class GestorDistribuicoes.
     */
    @Test
    public void testValorCaixa() {
        System.out.println("valorCaixa");
        double p = 0.0;
        GestorDistribuicoes instance = new GestorDistribuicoes(1000000);
        
        //Vetor:
        // 15.776,  26.101,  27.154,  32.307,  33.846,  34.792,  45.734,  51.801,  52.444,  53.131,  53.977,
        // 57.361,  64.148,  69.563,  69.986,  82.652,  91.698, 104.643, 116.985, 176.492
        
        assertEquals( instance.vetorCaixas[0], instance.gerarTempoAtendimentoEmCaixa(-1),1);
        assertEquals( instance.vetorCaixas[0], instance.gerarTempoAtendimentoEmCaixa(0),1);
        assertEquals( instance.vetorCaixas[0], instance.gerarTempoAtendimentoEmCaixa(0.0001),1);
        
        assertEquals( instance.vetorCaixas[1], instance.gerarTempoAtendimentoEmCaixa(1/19d),1);
        
        assertEquals( instance.vetorCaixas[4], instance.gerarTempoAtendimentoEmCaixa(4/19d),1);
                
        assertEquals( instance.vetorCaixas[7], instance.gerarTempoAtendimentoEmCaixa(7/19d),1);              
        assertEquals( instance.vetorCaixas[7], instance.gerarTempoAtendimentoEmCaixa((7/19d)+0.001),1);
        
        assertEquals( instance.vetorCaixas[15], instance.gerarTempoAtendimentoEmCaixa(15/19d),1);
        
        assertEquals( instance.vetorCaixas[19], instance.gerarTempoAtendimentoEmCaixa(1),1);
        assertEquals( instance.vetorCaixas[19], instance.gerarTempoAtendimentoEmCaixa(0.9999),1);
        
        
        //instance.printFuncaoCaixas();
    }
    
    /**
     *  Teste do calculoDaMediaCorrigidaComOMultiplicador()
     */
    @Test
    public void testCalculoDaMediaCorrigidaComOMultiplicador() {
        GestorDistribuicoes instance = new GestorDistribuicoes(1000000);
        
        double media = 0; long tempoAgora = 0 ; long tempoFim = 1;double mediaCorrigida = 0; double multiplicador = 5;
        double esperado = 0;
        
        
        media = 1;
        multiplicador = 2;
        tempoAgora = 0;
        tempoFim = 100;        
        for(int i = 0; i <= 100; i++){
            mediaCorrigida = instance.calculoDaMediaCorrigidaComOMultiplicador(media, multiplicador, i, tempoFim);
            System.out.println("Tempo: " + i + " y= " + mediaCorrigida);
        }
        
        
        
        media = 1;
        multiplicador = 2;
        tempoAgora = 0;
        tempoFim = 100;        
        mediaCorrigida = instance.calculoDaMediaCorrigidaComOMultiplicador(media, multiplicador, tempoAgora, tempoFim);
        
        for(int i = 0; i <= 50; i++){
            mediaCorrigida = instance.calculoDaMediaCorrigidaComOMultiplicador(media, multiplicador, i, tempoFim);
            esperado = 1d+ (((double) i)/tempoFim)*2;
            assertEquals(mediaCorrigida, esperado, 0.01);
            System.out.println(""+i);
        }
        
        media = 1;
        multiplicador = 2;
        tempoAgora = 75;
        tempoFim = 100;     
        esperado = 1d+0.5d ;
        mediaCorrigida = instance.calculoDaMediaCorrigidaComOMultiplicador(media, multiplicador, tempoAgora, tempoFim);
        assertEquals(mediaCorrigida, esperado, 0.01);
        
        media = 1;
        multiplicador = 2;
        tempoAgora = 100;
        tempoFim = 100;     
        esperado = 1d ;
        mediaCorrigida = instance.calculoDaMediaCorrigidaComOMultiplicador(media, multiplicador, tempoAgora, tempoFim);
        assertEquals(mediaCorrigida, esperado, 0.01);
        
        
        
        media = 1;
        multiplicador = 5;
        tempoAgora = 25;
        tempoFim = 100;     
        esperado = 3d ;
        mediaCorrigida = instance.calculoDaMediaCorrigidaComOMultiplicador(media, multiplicador, tempoAgora, tempoFim);
        assertEquals(mediaCorrigida, esperado, 0.01);
        
                media = 1;
        multiplicador = 5;
        tempoAgora = 75;
        tempoFim = 100;     
        esperado = 3d ;
        mediaCorrigida = instance.calculoDaMediaCorrigidaComOMultiplicador(media, multiplicador, tempoAgora, tempoFim);
        assertEquals(mediaCorrigida, esperado, 0.01);
        
        
        media = 1;
        multiplicador = 0.5;
        tempoAgora = 0;
        tempoFim = 100;        
        for(int i = 0; i <= 100; i++){
            mediaCorrigida = instance.calculoDaMediaCorrigidaComOMultiplicador(media, multiplicador, i, tempoFim);
            System.out.println("Tempo: " + i + " y= " + mediaCorrigida);
        }
        
        
        
        media = 1;
        multiplicador = 0.5;
        tempoAgora = 50;
        tempoFim = 100;     
        esperado = 0.5d ;
        mediaCorrigida = instance.calculoDaMediaCorrigidaComOMultiplicador(media, multiplicador, tempoAgora, tempoFim);
        assertEquals(mediaCorrigida, esperado, 0.01);
        
        
                media = 1;
        multiplicador = 0.5;
        tempoAgora = 75;
        tempoFim = 100;     
        esperado = 0.75d ;
        mediaCorrigida = instance.calculoDaMediaCorrigidaComOMultiplicador(media, multiplicador, tempoAgora, tempoFim);
        assertEquals(mediaCorrigida, esperado, 0.01);
    }
    
    
}
