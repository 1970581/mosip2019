/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.model;

import java.time.Duration;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class EventoTest {
    
    public EventoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class Evento.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        
        Evento a0 = new Evento(Duration.ofHours(2),0);
        Evento b0 = new Evento(Duration.ofHours(3),0);
        Evento b1 = new Evento(Duration.ofHours(3),1);
        
        assertTrue(a0.compareTo(b0) < 0);
        assertTrue(b0.compareTo(b1) > 0);
                
   
    }
    
}
