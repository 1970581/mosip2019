/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.motor;

import java.time.Duration;
import java.util.List;
import mosip2019.estatistica.Registo;
import mosip2019.model.Caixa;
import mosip2019.model.Cliente;
import mosip2019.model.Evento;
import mosip2019.model.EventoClienteCaixa;
import mosip2019.model.EventoClienteCaixaFim;
import mosip2019.model.EventoClienteEntra;
import mosip2019.model.EventoFim;

/**
 *
 * @author Hugo
 */
public class GestorOperacoes {
    
    
    public void tratarEventoFim(EventoFim evento){
        Estado.supermercado.relogio.SetDurationDeFim(evento.instante);
        System.out.println("Fim");
    }
    
    public void tratarEventoClienteEntra(EventoClienteEntra evento){
        GerarProximoCliente();
        Registo.addRegistoOcorrenciaDeEvento("Cliente "+ evento.cliente.id + " entrou as " + evento.instante.toString());
        Estado.supermercado.geradorRelatorio.registarClienteEntrada(evento.cliente);
        
        // Cliente entra e escolhe a caixa melhor e fica na fila da caixa.         
        Caixa caixa = obterCaixaComFilaMaisVaziaParaCliente(evento.cliente);
        caixa.adicionarClienteAFila(evento.cliente);
        
        //Se livre, vamos pedir a caixa para chamar o proximo cliente.
        if(caixa.isLivre()) chamarProximoClienteSeLivreCriandoEvento(caixa);       
        
    }
    
    public void tratarEventoClienteCaixa(EventoClienteCaixa evento){
        Caixa caixa = evento.caixa;
        //Cliente cliente = caixa.chamarProximoCliente();
        Cliente cliente = evento.cliente;
        
        
        if(evento.caixa == null || cliente == null){
            int i = 0; Registo.logEventos.isEmpty(); 
        }
        Registo.addRegistoOcorrenciaDeEvento("Cliente "+ cliente.id + " ocupou caixa "+ caixa.id +" as " + evento.instante.toString());
        Estado.supermercado.geradorRelatorio.registarClienteEntradaCaixa(cliente, caixa);
        
        long tempoAtendimentoLong = Estado.supermercado.gestorDistribuicoes.gerarTempoSegundosAtendimentoEmCaixa();
        if (tempoAtendimentoLong < 0) tempoAtendimentoLong = 0;        
        Duration tempoEvento = Estado.supermercado.relogio.getDuration().plusSeconds(tempoAtendimentoLong);
                
        EventoClienteCaixaFim eventoClienteCaixaFim = new EventoClienteCaixaFim(cliente,caixa, tempoEvento);
        Estado.supermercado.gestorEventos.AdicionarEvento(eventoClienteCaixaFim);
    }    
    
    public void tratarEventoClienteCaixaFim( EventoClienteCaixaFim evento){
        Caixa caixa = evento.caixa;
        Cliente cliente = evento.cliente;
        caixa.setLivre(evento.instante);
        Registo.addRegistoOcorrenciaDeEvento("Cliente "+ cliente.id + " saida da caixa "+ caixa.id +" as " + evento.instante.toString());
        Estado.supermercado.geradorRelatorio.registarClienteSaidaCaixa(cliente, caixa);
        
        //Se fila da caixa ainda tem clientes, chamamos o seguinte:
        if(caixa.tamanhoDaFila() > 0) chamarProximoClienteSeLivreCriandoEvento(caixa);        
    }
    
    
    
    // GESTAO:
    
    public static void GerarProximoCliente(){        
        
        //long tempoEsperaLong = Estado.supermercado.gestorDistribuicoes.gerarTempoExponencialSegundosEntreProximoCliente();
        long tempoEsperaLong = Estado.supermercado.gestorDistribuicoes.gerarTempoExponencialSegundosEntreProximoClienteCheckandoHoraDePonta();
        if (tempoEsperaLong < 0) tempoEsperaLong = 0;
        
        Duration tempoEvento = Estado.supermercado.relogio.getDuration().plusSeconds(tempoEsperaLong);
        Cliente novoCliente = new Cliente(Estado.supermercado.gestorDistribuicoes.isClientePrioritario());
        EventoClienteEntra eventoClienteEntra = new EventoClienteEntra(novoCliente, tempoEvento);
        Estado.supermercado.addCliente(novoCliente);
        Estado.supermercado.gestorEventos.AdicionarEvento(eventoClienteEntra);
    }
    
    public static Caixa obterCaixaComFilaMaisVaziaParaCliente(Cliente cliente){
        Caixa caixaMin = obterListaCaixasParaCliente(cliente).get(0);
        int min = caixaMin.tamanhoDaFila() + (caixaMin.isLivre() ? 0: 1);        
        
        for(Caixa c : Estado.supermercado.listaCaixas){
             
            int size = c.tamanhoDaFila() + (c.isLivre() ? 0: 1);
            
            if(size <= min) {
                caixaMin = c;
                min = size;
            }
            //System.out.println(c.id+" " + size + " "+c.isLivre()+ " " + c.tamanhoDaFila() + " min " + caixaMin.id);
        }
        //System.out.println("min " + caixaMin.id);
        return caixaMin;
            
    }
    
    private static List<Caixa> obterListaCaixasParaCliente(Cliente cliente){
        return Estado.supermercado.listaCaixas;
    }
    
    private static void chamarProximoClienteSeLivreCriandoEvento(Caixa caixa){
        if(caixa.isLivre()) {
            Cliente proximoCliente = caixa.chamarProximoCliente();
            EventoClienteCaixa eventoClienteCaixa = new EventoClienteCaixa(proximoCliente, caixa,Estado.supermercado.relogio.getDuration());
            caixa.setOcupada(Estado.supermercado.relogio.tempo);
            Estado.supermercado.gestorEventos.AdicionarEvento(eventoClienteCaixa);
        }
        else{
            System.out.println("********************************************   chamarProximoClienteSeLivreCriandoEvento em Caixa NAO LIVRE " + Estado.supermercado.relogio.tempo);
        }
    }
    
    
    
}
