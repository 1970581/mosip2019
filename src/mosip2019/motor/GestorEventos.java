/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.motor;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import mosip2019.model.EventoClienteEntra;
import mosip2019.model.Evento;
import mosip2019.model.EventoClienteCaixa;
import mosip2019.model.EventoClienteCaixaFim;
import mosip2019.model.EventoFim;

/**
 *
 * @author Hugo
 */
public class GestorEventos {
    
    public List<Evento> eventos = new ArrayList<Evento>();
    public List<Evento> eventosPassados = new ArrayList<Evento>();
            
    private void Adicionar(Evento evento){
        this.eventos.add(evento);
        Collections.sort(eventos);
    }
    
    public void AdicionarEvento(EventoClienteEntra evento){     this.Adicionar(evento);}    
    public void AdicionarEvento(EventoFim evento){              this.Adicionar(evento);}    
    public void AdicionarEvento(EventoClienteCaixa evento){     this.Adicionar(evento);}    
    public void AdicionarEvento(EventoClienteCaixaFim evento){  this.Adicionar(evento);}
    
    private Evento proximoEvento(){
        Evento e = this.eventos.remove(0);
        eventosPassados.add(e);
        return e;
    }
    
    public boolean hasEventos(){
        return !eventos.isEmpty();
    }
    
    public void indicarDurationFim(Duration tempoFim){
        EventoFim fim = new EventoFim(tempoFim);
        this.AdicionarEvento(fim);        
    }
    
    public void next(){  // Rotina de 
        if (eventos.isEmpty()) return;
        Evento e = this.proximoEvento();
        Estado.supermercado.relogio.AdvanceTo(e.instante);
        
        if(e instanceof EventoFim){
            Estado.supermercado.gestorOperacoes.tratarEventoFim((EventoFim) e);
        }
        
        if(e instanceof EventoClienteEntra){
            Estado.supermercado.gestorOperacoes.tratarEventoClienteEntra((EventoClienteEntra) e);
        }
        
        if(e instanceof EventoClienteCaixa){
            Estado.supermercado.gestorOperacoes.tratarEventoClienteCaixa((EventoClienteCaixa) e);
        }
        
        if(e instanceof EventoClienteCaixaFim){
            Estado.supermercado.gestorOperacoes.tratarEventoClienteCaixaFim((EventoClienteCaixaFim) e);
        }
        
    }
    
    
}
