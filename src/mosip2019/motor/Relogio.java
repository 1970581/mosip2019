/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.motor;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.TemporalAmount;

/**
 *
 * @author Hugo
 */
public class Relogio {
    
    
    public Duration tempo;
    public Duration tempoDeFim;
    public boolean fim = false;
    
    public Relogio(Duration duracaoDaSimulacao){
        tempo = Duration.ofHours(0); //h,m,s,ms
        tempoDeFim = Duration.from(duracaoDaSimulacao);
    }
    
    public void SetDurationDeFim(Duration tempo){
        tempoDeFim = Duration.from(tempo);
        checkTempoFim();
    }
    
    public boolean checkTempoFim(){
        if(tempo.compareTo(tempoDeFim) >= 0 ) fim = true;
        return fim;
    }
    
//    public  void AdvanceXSeconds(int seconds){
//        tempo = tempo.plusSeconds(seconds);
//        checkTempoFim();
//    }
    
    public  void AdvanceTo(Duration newTempo){
         //TemporalAmount.addTo(addTempo)
        //addTempo. TemporalAmount
        tempo = Duration.from(newTempo);
        checkTempoFim();
    }
    
    public Duration getDuration(){
        return Duration.from(tempo);
    }   
    
    
}
