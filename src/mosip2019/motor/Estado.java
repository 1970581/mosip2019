/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.motor;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import mosip2019.estatistica.GestorDistribuicoes;
import mosip2019.estatistica.RelatorioSimulacao;
import mosip2019.model.Supermercado;

/**
 *
 * @author Hugo
 */
public class Estado {
    
    public static Supermercado supermercado;  
    
    public static GestorDistribuicoes gestorDistribuicoes;
    public static List<RelatorioSimulacao> listaRelatorios = new ArrayList<RelatorioSimulacao>();
    
    public static void clear(){
        supermercado = null;
        gestorDistribuicoes = null;
        listaRelatorios.clear();
    }
    
    public static void adicionarRelatorio(RelatorioSimulacao relatorio){
        listaRelatorios.add(relatorio);
    }
    
    public static void imprimirRelatorios(){
        System.out.println(RelatorioSimulacao.getHeader());
        for(RelatorioSimulacao r : listaRelatorios){
            System.out.println(r.toString());            
        }
    }
    
    public static boolean escreverRelatorio(File myFile){
        if(listaRelatorios.isEmpty()) return false;
        if(myFile == null) return false;
        
        try{
        //File myFile = new File(filename);
        PrintWriter pw = new PrintWriter(myFile);
        
        String header = RelatorioSimulacao.getHeader();
        pw.println(header);
        for (RelatorioSimulacao r : listaRelatorios) {
            pw.println(r.toString());
        }
        pw.flush();
        pw.close();
        }
        catch(Exception e){
            System.out.println("ERROR: "+ e.getMessage());
            return false;
        }
        return true;
    }
    
    public static String getReportName(){
        Date dataInicio = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss_");
        String first = formatter.format(dataInicio);
        //String last = nome.replaceAll("[^a-zA-Z0-9]", "");
        
        
        
        String filename = first + "simulacao_x"+ listaRelatorios.size() +".txt";
        return filename;
    }
    
    public static boolean salvarGraficoPontosMediaClientesEspera(File myFile){
        if(listaRelatorios.isEmpty()) return false;
        if(supermercado == null) return false;
        if(myFile == null) return false;
        
        try{
        //File myFile = new File(filename);
        PrintWriter pw = new PrintWriter(myFile);
        
        String header = "temposeg;clientes";
        pw.println(header);
        for (Object o : supermercado.geradorRelatorio.getPontos()) {
            pw.println(o.toString());
        }
        pw.flush();
        pw.close();
        }
        catch(Exception e){
            System.out.println("ERROR: "+ e.getMessage());
            return false;
        }
        return true;
    }
    
    public static String getFileNameGraficoPontosMediaClientesEspera(){
        Date dataInicio = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss_");
        String first = formatter.format(dataInicio);
        //String last = nome.replaceAll("[^a-zA-Z0-9]", "");
        
        
        
        String filename = first + "s_x"+ listaRelatorios.size() + "_grafico"+".txt";
        return filename;
    }
    
}
