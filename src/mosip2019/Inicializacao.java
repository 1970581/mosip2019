/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019;

import java.time.Duration;
import mosip2019.estatistica.GestorDistribuicoes;
import mosip2019.estatistica.Registo;
import mosip2019.model.Supermercado;
import mosip2019.motor.Estado;

/**
 *
 * @author Hugo
 */
public class Inicializacao {
    
    //public Duration duracaoDaSimulacao = Duration.ofHours(4);
    
    
    public void iniciar(boolean isSingleRun, int numeroRuns, long mySeed, Duration duracaoDaSimulacao
                    , boolean isMonoFila, int numeroCaixas
                    , double tempoMedioEntreChegadaClientes
                    , double taxaDePrioritarios
                    , boolean usarHoraPonta, double multiplicadorHoraPonta
    ){
        Estado.clear();
        Estado.gestorDistribuicoes = new GestorDistribuicoes(mySeed);
        Estado.gestorDistribuicoes.setTemposMedioEntreChegadasSeg(tempoMedioEntreChegadaClientes);
        Estado.gestorDistribuicoes.setTaxaPrioritarios(taxaDePrioritarios);
        if(usarHoraPonta) Estado.gestorDistribuicoes.setMultiplicadorHoraPontaAndTurnTrue(multiplicadorHoraPonta);
        
        Registo.clearRegisto();
        
        int nRuns = numeroRuns;
        if(isSingleRun) nRuns = 1;
        
        //Particular para cada run:
        //System.out.println("\007");        //BEEP
        
        for(int i = 0; i < nRuns; i++){
            Supermercado supermercado = new Supermercado(duracaoDaSimulacao, isMonoFila, numeroCaixas, numeroCaixas);
            Estado.supermercado = supermercado;
            supermercado.correrSimulacao();    
        }
        
        Estado.imprimirRelatorios();
        
    }
    
}
