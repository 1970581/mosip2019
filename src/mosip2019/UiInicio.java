/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019;

import java.io.File;
import java.time.Duration;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import mosip2019.estatistica.GestorDistribuicoes;
import mosip2019.estatistica.Registo;
import mosip2019.motor.Estado;

/**
 *
 * @author Hugo
 */
public class UiInicio extends javax.swing.JFrame {

    Inicializacao inicializacao;
    
    /**
     * Creates new form UiInicio
     */
    public UiInicio() {
        initComponents();
        this.jCheckBoxSingle.setSelected(true);
        //this.jButtonDebugPrint.color
        
        this.jTextFieldTaxaPrioritarios.setText(""+GestorDistribuicoes.TAXA_PRIORITARIOS);
        this.jTextFieldTempoMedioChegada.setText(""+GestorDistribuicoes.TEMPO_MEDIO_ENTRE_CHEGADAS_SEG);
    }
    
    public void Correr(){
        
        boolean isSingleSimulacao = this.jCheckBoxSingle.isSelected();
        boolean isMonofila = this.jCheckBoxMonofila.isSelected();
        
        long durationLong = 13*60;
        try{
            durationLong = Long.parseLong(this.jTextFieldNumeroDuracao.getText());
            if (durationLong <1) throw new Exception();
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog( this,
                "Invalid duration: " + this.jTextFieldNumeroDuracao.getText(),
                "PArse error",  JOptionPane.ERROR_MESSAGE);
            return;
        }
        Duration duracao = Duration.ofMinutes(durationLong);
        
        int numeroCaixas = 5;
        try{
            numeroCaixas = Integer.parseInt(this.jTextFieldNumeroCaixas.getText());
            if (numeroCaixas <1) throw new Exception();
        }
        catch(Exception ex2){
            JOptionPane.showMessageDialog( this,
                "Invalid numero de caixas: " + this.jTextFieldNumeroCaixas.getText(),
                "PArse error",  JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        int numeroSimulacoes = 1;
        try{
            numeroSimulacoes = Integer.parseInt(this.jTextFieldNumeroRuns.getText());
            if (numeroSimulacoes <1) throw new Exception();
        }
        catch(Exception ex3){
            JOptionPane.showMessageDialog( this,
                "Invalid numero de simulacoes: " + this.jTextFieldNumeroRuns.getText(),
                "PArse error",  JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        long mySeed = 1;
        try{
            mySeed = Long.parseLong(this.jTextFieldNumeroSeed.getText());
            if (mySeed <1) throw new Exception();
        }
        catch(Exception ex4){
            JOptionPane.showMessageDialog( this,
                "Invalid numero para Seed: " + this.jTextFieldNumeroSeed.getText(),
                "PArse error",  JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        
        
        double myTempoMedioEntreChegadas = GestorDistribuicoes.TEMPO_MEDIO_ENTRE_CHEGADAS_SEG;
        try{
            myTempoMedioEntreChegadas = Double.parseDouble(this.jTextFieldTempoMedioChegada.getText());
            if (myTempoMedioEntreChegadas <0) throw new Exception();
        }
        catch(Exception ex5){
            JOptionPane.showMessageDialog( this,
                "Invalid numero para tempo medio de chegada: " + this.jTextFieldTempoMedioChegada.getText(),
                "PArse error",  JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        double myTaxaPrioritarios = GestorDistribuicoes.TAXA_PRIORITARIOS;
        try{
            myTaxaPrioritarios = Double.parseDouble(this.jTextFieldTaxaPrioritarios.getText());
            if (myTaxaPrioritarios <0 || myTaxaPrioritarios > 1) throw new Exception();
        }
        catch(Exception ex6){
            JOptionPane.showMessageDialog( this,
                "Invalid numero para taxa de prioritarios: " + this.jTextFieldTaxaPrioritarios.getText(),
                "PArse error",  JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        boolean isParaUsarHoraDePonta = this.jCheckBoxHoraPonta.isSelected();
        
        double myMultiplicadorHoraPonta = 1;
        try{
            myMultiplicadorHoraPonta = Double.parseDouble(this.jTextFieldPontaMultiplicador.getText());
            if (myMultiplicadorHoraPonta <0) throw new Exception();
        }
        catch(Exception ex7){
            JOptionPane.showMessageDialog( this,
                "Invalid numero para multiplicador hora de ponta: " + this.jTextFieldPontaMultiplicador.getText(),
                "PArse error",  JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        this.inicializacao = new Inicializacao();
        inicializacao.iniciar(isSingleSimulacao, numeroSimulacoes, mySeed, duracao, isMonofila, numeroCaixas
                ,myTempoMedioEntreChegadas
                , myTaxaPrioritarios
                , isParaUsarHoraDePonta, myMultiplicadorHoraPonta
        );
    }
    
    public void doDebugPrinting(){
        Registo.printAll();
    }
    
    public void imprimirRelatorio(){
        String sugestao = Estado.getReportName();
        
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Ficheiros de Texto", "txt");
        chooser.setFileFilter(filter);
        chooser.setSelectedFile(new File(sugestao));
        int returnVal = chooser.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("You chose to open this file: "
                    + chooser.getSelectedFile().getName());
        }    
        
        boolean pass = Estado.escreverRelatorio(chooser.getSelectedFile());
        if (!pass){
            JOptionPane.showMessageDialog( this,
                "Erro a guardar ficheiro " +chooser.getSelectedFile().getName(),
                "File save erro",  JOptionPane.ERROR_MESSAGE);
            return;
        }
    }

    public void salvarGrafico(){
        String sugestao = Estado.getFileNameGraficoPontosMediaClientesEspera();
        
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Ficheiros de Texto", "txt");
        chooser.setFileFilter(filter);
        chooser.setSelectedFile(new File(sugestao));
        int returnVal = chooser.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("You chose to open this file: "
                    + chooser.getSelectedFile().getName());
        }    
        
        boolean pass = Estado.salvarGraficoPontosMediaClientesEspera(chooser.getSelectedFile());
        if (!pass){
            JOptionPane.showMessageDialog( this,
                "Erro a guardar ficheiro " +chooser.getSelectedFile().getName(),
                "File save erro",  JOptionPane.ERROR_MESSAGE);
            return;
        }
    }    
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jCheckBoxSingle = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldNumeroRuns = new javax.swing.JTextField();
        jButtonStart = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jCheckBoxMonofila = new javax.swing.JCheckBox();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldNumeroCaixas = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldNumeroDuracao = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextFieldNumeroSeed = new javax.swing.JTextField();
        jButtonDebugPrint = new javax.swing.JButton();
        jButtonImprimirRelatorio = new javax.swing.JButton();
        jButtonImprimirGrafico = new javax.swing.JButton();
        jCheckBoxHoraPonta = new javax.swing.JCheckBox();
        jTextFieldTempoMedioChegada = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldTaxaPrioritarios = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextFieldPontaMultiplicador = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Simulador de Supermercado");

        jCheckBoxSingle.setText("Apenas 1 simulação");
        jCheckBoxSingle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSingleActionPerformed(evt);
            }
        });

        jLabel2.setText("Runs:");

        jTextFieldNumeroRuns.setText("1");
        jTextFieldNumeroRuns.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNumeroRunsActionPerformed(evt);
            }
        });

        jButtonStart.setBackground(new java.awt.Color(204, 255, 204));
        jButtonStart.setText("Start!");
        jButtonStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonStartActionPerformed(evt);
            }
        });

        jLabel3.setText("Supermercado:");

        jLabel4.setText("Simulação:");

        jCheckBoxMonofila.setText("Monofila");
        jCheckBoxMonofila.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMonofilaActionPerformed(evt);
            }
        });

        jLabel5.setText("Caixas:");

        jTextFieldNumeroCaixas.setText("5");
        jTextFieldNumeroCaixas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNumeroCaixasActionPerformed(evt);
            }
        });

        jLabel6.setText("Duracao (min):");

        jTextFieldNumeroDuracao.setText("660");

        jLabel7.setText("Seed:");

        jTextFieldNumeroSeed.setText("1200000");
        jTextFieldNumeroSeed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNumeroSeedActionPerformed(evt);
            }
        });

        jButtonDebugPrint.setBackground(new java.awt.Color(255, 51, 51));
        jButtonDebugPrint.setText("Debug Print");
        jButtonDebugPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDebugPrintActionPerformed(evt);
            }
        });

        jButtonImprimirRelatorio.setText("Imprimir Relatorio");
        jButtonImprimirRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonImprimirRelatorioActionPerformed(evt);
            }
        });

        jButtonImprimirGrafico.setBackground(new java.awt.Color(255, 255, 204));
        jButtonImprimirGrafico.setText("Imprimir Grafico Clientes Espera");
        jButtonImprimirGrafico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonImprimirGraficoActionPerformed(evt);
            }
        });

        jCheckBoxHoraPonta.setText("Usar horas de ponta");

        jTextFieldTempoMedioChegada.setText("16.43026");

        jLabel8.setText("Tempo de chegada médio (seg):");

        jTextFieldTaxaPrioritarios.setText("0");
        jTextFieldTaxaPrioritarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTaxaPrioritariosActionPerformed(evt);
            }
        });

        jLabel9.setText("Taxa de prioritarios (0 a 1):");

        jTextFieldPontaMultiplicador.setText("0.75");
        jTextFieldPontaMultiplicador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldPontaMultiplicadorActionPerformed(evt);
            }
        });

        jLabel10.setText("Multiplicador de tempo medio de chegada:   x");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCheckBoxSingle, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldNumeroRuns, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldNumeroSeed, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jTextFieldNumeroDuracao, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jCheckBoxMonofila)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextFieldNumeroCaixas, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jCheckBoxHoraPonta)
                                                .addGap(22, 22, 22))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(jLabel8)
                                                    .addComponent(jLabel9)
                                                    .addComponent(jLabel10))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jTextFieldPontaMultiplicador, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                                                        .addComponent(jTextFieldTaxaPrioritarios, javax.swing.GroupLayout.Alignment.TRAILING))
                                                    .addComponent(jTextFieldTempoMedioChegada, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(12, 12, 12))))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jButtonStart)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonImprimirRelatorio, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonDebugPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(20, 20, 20))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonImprimirGrafico)
                .addGap(127, 127, 127))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxSingle)
                    .addComponent(jCheckBoxHoraPonta))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldNumeroRuns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPontaMultiplicador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextFieldNumeroDuracao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jTextFieldNumeroSeed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldTempoMedioChegada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextFieldTaxaPrioritarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxMonofila)
                    .addComponent(jLabel5)
                    .addComponent(jTextFieldNumeroCaixas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonStart)
                    .addComponent(jButtonDebugPrint)
                    .addComponent(jButtonImprimirRelatorio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonImprimirGrafico)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(107, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(81, 81, 81)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(75, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxSingleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSingleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxSingleActionPerformed

    private void jTextFieldNumeroRunsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNumeroRunsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNumeroRunsActionPerformed

    private void jButtonStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonStartActionPerformed
        // TODO add your handling code here:
        this.Correr();
        
    }//GEN-LAST:event_jButtonStartActionPerformed

    private void jCheckBoxMonofilaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMonofilaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxMonofilaActionPerformed

    private void jTextFieldNumeroCaixasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNumeroCaixasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNumeroCaixasActionPerformed

    private void jTextFieldNumeroSeedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNumeroSeedActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNumeroSeedActionPerformed

    private void jButtonDebugPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDebugPrintActionPerformed
        // TODO add your handling code here:
        this.doDebugPrinting();
    }//GEN-LAST:event_jButtonDebugPrintActionPerformed

    private void jButtonImprimirRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonImprimirRelatorioActionPerformed
        // TODO add your handling code here:
        this.imprimirRelatorio();
    }//GEN-LAST:event_jButtonImprimirRelatorioActionPerformed

    private void jButtonImprimirGraficoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonImprimirGraficoActionPerformed
        // TODO add your handling code here:
        this.salvarGrafico();
    }//GEN-LAST:event_jButtonImprimirGraficoActionPerformed

    private void jTextFieldTaxaPrioritariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTaxaPrioritariosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTaxaPrioritariosActionPerformed

    private void jTextFieldPontaMultiplicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldPontaMultiplicadorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldPontaMultiplicadorActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UiInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UiInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UiInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UiInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UiInicio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonDebugPrint;
    private javax.swing.JButton jButtonImprimirGrafico;
    private javax.swing.JButton jButtonImprimirRelatorio;
    private javax.swing.JButton jButtonStart;
    private javax.swing.JCheckBox jCheckBoxHoraPonta;
    private javax.swing.JCheckBox jCheckBoxMonofila;
    private javax.swing.JCheckBox jCheckBoxSingle;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextFieldNumeroCaixas;
    private javax.swing.JTextField jTextFieldNumeroDuracao;
    private javax.swing.JTextField jTextFieldNumeroRuns;
    private javax.swing.JTextField jTextFieldNumeroSeed;
    private javax.swing.JTextField jTextFieldPontaMultiplicador;
    private javax.swing.JTextField jTextFieldTaxaPrioritarios;
    private javax.swing.JTextField jTextFieldTempoMedioChegada;
    // End of variables declaration//GEN-END:variables
}
