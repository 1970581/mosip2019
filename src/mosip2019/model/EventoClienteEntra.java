/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.model;

import java.time.Duration;
import java.time.LocalTime;
import mosip2019.estatistica.Registo;

/**
 *
 * @author Hugo
 */
public class EventoClienteEntra extends Evento{
    
    
    public final Cliente cliente;
    
    public EventoClienteEntra(Cliente c, Duration instante){
        super(instante, Evento.PRIORITY_CLIENTE_ENTRA);
        this.cliente = c;    
        String myStr = this.getClass().getName() 
                + " criado para instante "
                + super.instante.toString()
                + " cliente ID: "
                + c.id
                ;
                Registo.addRegistoCriacaoEvento(myStr);
    }
    




    
}
