/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.model;

import java.time.Duration;
import mosip2019.estatistica.Registo;

/**
 *
 * @author Hugo
 */
public class EventoClienteCaixaFim extends Evento{
    
    public final Cliente cliente;
    public final Caixa caixa;
    
    public EventoClienteCaixaFim(Cliente cliente, Caixa caixa, Duration instante){
        super(instante, Evento.PRIORITY_CLIENTE_CAIXA_FIM);
        this.cliente = cliente;  
        this.caixa = caixa;
        String myStr = this.getClass().getName() 
                + " criado para instante "
                + super.instante.toString()
                + " cliente ID: "
                + cliente.id
                + " caixa ID: "
                + caixa.id
                ;
                Registo.addRegistoCriacaoEvento(myStr);
    }
}
