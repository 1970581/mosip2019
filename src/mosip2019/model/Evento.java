/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.model;

import java.time.Duration;
import java.time.LocalTime;

/**
 *
 * @author Hugo
 */
public class Evento implements Comparable<Evento>{
    public Duration instante;
    public final long id;
    public static long lastId = -1;
    public final int priorityCrescente;
    
    //Prioridades crescente. Alto ==> mais prioridade;
    public static final int PRIORITY_CLIENTE_ENTRA = 100;//1; //100
    public static final int PRIORITY_CLIENTE_CAIXA_FIM = 2;//3;//2
    public static final int PRIORITY_CLIENTE_CAIXA = 1;//2; //1
    public static final int PRIORITY_FIM_SIMULACAO = 0; //0
    
    public Evento(){
        this.instante = Duration.ofHours(0); //h,m,s,ms
        this.priorityCrescente = PRIORITY_CLIENTE_ENTRA;
        ++lastId;
        this.id = lastId;
    }
    
    public Evento(Duration tempo, int priority ) {
        this.instante = Duration.from(tempo);
        this.priorityCrescente = priority;
        ++lastId;
        this.id = lastId;
        
    }
    
    @Override
    public int compareTo(Evento o) {
        int comp = this.instante.compareTo(o.instante);
        if (comp == 0) comp = o.priorityCrescente - this.priorityCrescente;
        return comp;
    }
    
}
