/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.model;

import java.time.Duration;

/**
 *
 * @author Hugo
 */
public class Caixa {
    
    public static long lastId = -1;
    public final long id;
    public final Fila fila;
    private boolean livre = true;
    public Cliente clienteEmCaixa;

    //Registos:
    private Duration lastTime;
    public Duration duracaoLivre;
    public Duration duracaoOcupada;
    
    
    
    
    public Caixa(Fila aFilaDaCaixa) {
        lastId++;
        this.id = lastId;
        fila = aFilaDaCaixa;
        this.lastTime = Duration.ofMinutes(0);
        duracaoLivre = Duration.ofMinutes(0);
        duracaoOcupada = Duration.ofMinutes(0);
    }

    public boolean isLivre(){return livre;}
    
    public void setOcupada(Duration tempo){
        this.livre = false;
        Duration eventDuration = tempo.minus(lastTime);
        duracaoLivre = duracaoLivre.plus(eventDuration);
        lastTime = Duration.from(tempo);        
    }
    
    
    public void setLivre(Duration tempo){
        this.livre = true;
        this.clienteEmCaixa = null;
        Duration eventDuration = tempo.minus(lastTime);
        duracaoOcupada = duracaoOcupada.plus(eventDuration);
        lastTime = Duration.from(tempo);        
    }
    
    public void finalizar(Duration tempo){
        Duration eventDuration = tempo.minus(lastTime);
        if (livre) {
            duracaoLivre = duracaoLivre.plus(eventDuration);
        } else {
            duracaoOcupada = duracaoOcupada.plus(eventDuration);
        }
        lastTime = Duration.from(tempo);
    }
    
    
    public void    adicionarClienteAFila(Cliente cliente){
        this.fila.addCliente(cliente);
    }
    
    public int tamanhoDaFila(){
        return this.fila.getSize();
    }

    public Cliente chamarProximoCliente() {
        if(fila.isEmpty()) return null;
        this.clienteEmCaixa = fila.getNextCliente();
        return clienteEmCaixa;
    }
    
}
