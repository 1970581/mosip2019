/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.model;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import mosip2019.estatistica.GeradorRelatorios;
import mosip2019.estatistica.GestorDistribuicoes;
import mosip2019.motor.Estado;
import mosip2019.motor.GestorEventos;
import mosip2019.motor.GestorOperacoes;
import mosip2019.motor.Relogio;

/**
 *
 * @author Hugo
 */
public class Supermercado {
    
    public Relogio relogio;
    public GestorEventos gestorEventos;    
    public GestorOperacoes gestorOperacoes;
    public GestorDistribuicoes gestorDistribuicoes; 
    public List<Cliente> listaClientes;
    public List<Caixa> listaCaixas;
    public GeradorRelatorios geradorRelatorio;
    public Fila filaUnica;
    
    
    public Supermercado(Duration duracaoDaSimulacao,boolean isMonoFila, int nCaixasComFilaPropria, int nCaixasMonofila){                
        relogio = new Relogio(duracaoDaSimulacao);  // Iniciar o relogio.        
        gestorEventos = new GestorEventos();
        gestorOperacoes = new GestorOperacoes();
        gestorDistribuicoes = Estado.gestorDistribuicoes;
        listaClientes = new ArrayList<Cliente>();
        listaCaixas = new ArrayList<Caixa>();
        geradorRelatorio = new GeradorRelatorios();
        
        if (isMonoFila) {
            
            for (int i = 0; i < nCaixasMonofila; i++) {
                this.criarCaixaComFilaUnica();
            }
        } else {            
            for (int i = 0; i < nCaixasComFilaPropria; i++) {
                this.criarCaixaComFila();
            }
        }
        
    }   
    
    public void criarPrimeiroCliente(){
        this.geradorRelatorio.registarGraficoClientesEsperaIniciar();
        GestorOperacoes.GerarProximoCliente();
    }
    
    public void fecharCaixas(){
        for(Caixa c : listaCaixas){
            c.finalizar(relogio.tempoDeFim);
            this.geradorRelatorio.adicionarCaixaFechada(c);
        }
    }
    
    
    public void criarCaixaComFila(){
        Fila fila = new Fila();
        Caixa caixa = new Caixa(fila);
        listaCaixas.add(caixa);
    }
    
    public boolean existeFilaUnica(){
        if(this.filaUnica == null) return true;
        else return false;
    }
    
    public void criarCaixaComFilaUnica(){
        if(this.filaUnica == null) this.filaUnica = new Fila();
        Caixa caixa = new Caixa(this.filaUnica);
        listaCaixas.add(caixa);
    }
    
    
    public void correrSimulacao(){
                
        this.criarPrimeiroCliente();
        
        while(gestorEventos.hasEventos() && !relogio.fim ){
            gestorEventos.next();
        }        
        
        this.fecharCaixas();
        this.geradorRelatorio.registarGraficoClientesEsperaFinalizar();
        Estado.adicionarRelatorio(this.geradorRelatorio.gerarRelatorioFinal());
    }
    
    
    public void addCliente(Cliente cliente){
        this.listaClientes.add(cliente);
    }
    
    public void setDurationFim(Duration fim){
        relogio.SetDurationDeFim(fim);
        gestorEventos.indicarDurationFim(fim);
    }

    
    // CAIXAS:
    
    public Caixa existeCaixaLivre(Cliente cliente) {
        for(Caixa c : listaCaixas){
            if (c.isLivre()) return c;
        }
        return null;
    }
    
    
    
}
