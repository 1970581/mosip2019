/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hugo
 */
public class Fila {
    
    public static int lastId = -1;
    public final int id;
    
   public Fila(){
       lastId++;
       this.id = lastId; 
   }
    
    
    public List<Cliente> fila = new ArrayList<Cliente>();
    
    public void addCliente(Cliente cliente){
        
        if(fila.isEmpty() || !cliente.clientePrioritario) {
            fila.add(cliente); 
            return;
        }
        
        int indexUltimoPrioritario = 0;        
        for(Cliente c :fila ){
            if (c.clientePrioritario) indexUltimoPrioritario = fila.indexOf(c);
        }
        fila.add(indexUltimoPrioritario +1, cliente);
    }
    
    public Cliente getNextCliente(){ return fila.remove(0); }
    
    public int getSize(){ return this.fila.size();}
    
    public boolean isEmpty(){return this.fila.isEmpty();}
    
}
