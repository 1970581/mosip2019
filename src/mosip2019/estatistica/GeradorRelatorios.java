/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.estatistica;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mosip2019.model.Caixa;
import mosip2019.model.Cliente;
import mosip2019.motor.Estado;

/**
 *
 * @author Hugo
 */
public class GeradorRelatorios {
    
    private List<ClienteRegisto> listaClientesFinalizados = new ArrayList<ClienteRegisto>();    
    private Map<Long,ClienteRegisto> mapaClientes = new HashMap<Long,ClienteRegisto>();
    private List<CaixaRegisto> listaCaixas = new ArrayList<CaixaRegisto>();
    private List<Ponto> listaGraficoClientesEspera = new ArrayList<Ponto>();
    private List<Ponto> auxListaGraficoClientesEspera = new ArrayList<Ponto>();
    private List<Ponto> finalListaGraficoClientesEspera = new ArrayList<Ponto>();
    
    private int numeroClientes = 0;
    private long ultimoEventoSegundos = 0;
    private long auxMediaClientesEspera = 0;
    private double finalMediaClientesEspera = 0;
    
    
    public void registarGraficoClientesEsperaIniciar(){
        long tempoAgora = Estado.supermercado.relogio.getDuration().getSeconds();
        listaGraficoClientesEspera.add(new Ponto(tempoAgora,0));
        ultimoEventoSegundos = tempoAgora;
    }
    public void registarGraficoClientesEsperaIncrementar1(){
        long tempoAgora = Estado.supermercado.relogio.getDuration().getSeconds();
        //listaGraficoClientesEspera.add(new Ponto(tempoAgora,numeroClientes));
        numeroClientes++;
        listaGraficoClientesEspera.add(new Ponto(tempoAgora,numeroClientes));        
        auxMediaClientesEspera += numeroClientes * (tempoAgora - ultimoEventoSegundos);  
        ultimoEventoSegundos = tempoAgora;
    }
    public void registarGraficoClientesEsperaDecrementar1(){
        long tempoAgora = Estado.supermercado.relogio.getDuration().getSeconds();
        //listaGraficoClientesEspera.add(new Ponto(tempoAgora,numeroClientes));
        numeroClientes--;
        listaGraficoClientesEspera.add(new Ponto(tempoAgora,numeroClientes));        
        auxMediaClientesEspera += numeroClientes * (tempoAgora - ultimoEventoSegundos);        
        ultimoEventoSegundos = tempoAgora;
    }
    public void registarGraficoClientesEsperaFinalizar(){
        long tempoAgora = Estado.supermercado.relogio.getDuration().getSeconds();
        listaGraficoClientesEspera.add(new Ponto(tempoAgora,numeroClientes));
        auxMediaClientesEspera += numeroClientes * (tempoAgora - ultimoEventoSegundos);
        finalMediaClientesEspera = auxMediaClientesEspera / (double)tempoAgora;
        
        // Corrigir o grafico para ter monopontos e duplicar os monopontos.
        processarGraficoClientesEspera();
    }
    
    public void processarGraficoClientesEspera(){
        int mySize = listaGraficoClientesEspera.size();
        if (mySize == 0) return;
        
        for(int i = 0; i < mySize; i++){
            Ponto p = listaGraficoClientesEspera.get(i);            
            if( (i+1) < mySize){
                // Se os tempos nao forem os mesmos 
                if(p.x != listaGraficoClientesEspera.get(i+1).x){
                    auxListaGraficoClientesEspera.add(p);
                }
            }            
        }
        auxListaGraficoClientesEspera.add(listaGraficoClientesEspera.get(mySize-1) );
        processarInsersaoDoDuplo();
    }
    
    public void processarInsersaoDoDuplo(){
        int mySize = auxListaGraficoClientesEspera.size();
        if (mySize == 0) return;
        
        for(int i = 0; i < mySize; i++){
            Ponto p = auxListaGraficoClientesEspera.get(i);            
            finalListaGraficoClientesEspera.add(p);
            if( (i+1) < mySize){
                
                Ponto pProximo = auxListaGraficoClientesEspera.get(i+1);
                // Marcamos novo ponto, no tempo do proximo com o valor do anterior.
                Ponto pIntermedio = new Ponto(pProximo.x, p.y);
                finalListaGraficoClientesEspera.add(pIntermedio);
            }            
        }
    }
    
    
    public void registarClienteEntrada(Cliente c){
       mapaClientes.put(c.id,new ClienteRegisto(Estado.supermercado.relogio.getDuration(), c.id));
       this.registarGraficoClientesEsperaIncrementar1();
       
    }
    
    public void registarClienteEntradaCaixa(Cliente cliente, Caixa caixa){
        ClienteRegisto registo = mapaClientes.get(cliente.id);
        registo.idCaixa = caixa.id;
        registo.tempoEntradaCaixa = Estado.supermercado.relogio.getDuration().getSeconds();
        this.registarGraficoClientesEsperaDecrementar1();
    }
    
    public void registarClienteSaidaCaixa(Cliente cliente, Caixa caixa){
        ClienteRegisto registo = mapaClientes.get(cliente.id);
        if(registo.idCaixa == caixa.id){
            registo.tempoSaidaCaixa = Estado.supermercado.relogio.getDuration().getSeconds();
            registo.finished = true;
            listaClientesFinalizados.add(registo);
        }
    }
    
    
    public void adicionarCaixaFechada(Caixa caixa){
        this.listaCaixas.add(new CaixaRegisto(caixa.id,caixa.duracaoLivre, caixa.duracaoOcupada));
    }
    
    
    public RelatorioSimulacao gerarRelatorioFinal(){
        
        //Registo.printAll();
        
        double tempoMedioDeEsperaClientesEmFila = 0;
        double tempoMedioDeEsperaClientesEmCaixa = 0;
        double tempoMaximoEsperaFila = 0;
        
        for(ClienteRegisto r : listaClientesFinalizados){
            tempoMedioDeEsperaClientesEmFila += (r.tempoEntradaCaixa - r.tempoEntrada);
            tempoMedioDeEsperaClientesEmCaixa += (r.tempoSaidaCaixa - r.tempoEntradaCaixa);            
            if ((r.tempoEntradaCaixa - r.tempoEntrada) > tempoMaximoEsperaFila) tempoMaximoEsperaFila = (r.tempoEntradaCaixa - r.tempoEntrada);
        }
        
        int nClients = listaClientesFinalizados.size();
        tempoMedioDeEsperaClientesEmFila /= nClients;
        tempoMedioDeEsperaClientesEmCaixa /= nClients;
        
        System.out.println("Clientes terminados: " + nClients);
        System.out.println("Media Clientes em fila: " + String.format("%.2f",finalMediaClientesEspera));
        System.out.println("Tempo maximo de espera: " + String.format("%.2f",tempoMaximoEsperaFila/60d) + " min");
        System.out.println("Tempo medio de espera: " + String.format("%.2f",tempoMedioDeEsperaClientesEmFila/60d) + " min");
        System.out.println("Tempo medio de atendimento: " + String.format("%.2f",tempoMedioDeEsperaClientesEmCaixa/60d) + " min");
        
        
        
        //Caixas:
        
        double tempoMinutosCaixasTotalOcupadas = 0;
        double tempoMinutosCaixasTotalLivres = 0;
        double taxaOcupacaoCaixasTotal = 0;
        
        for(CaixaRegisto cr : listaCaixas){
            
            tempoMinutosCaixasTotalOcupadas += cr.tempoSegundosOcupada/60d;
            tempoMinutosCaixasTotalLivres += cr.tempoSegundosLivre/60d;
            
            System.out.println( "#"+ String.format("%02d", cr.id) +
                    " Taxa ocupacao: " + String.format("%.2f", cr.taxaOcupacao) +
                    " Tempo ocupado: " + String.format("%.2f", (cr.tempoSegundosOcupada/60d)) +
                    " min Tempo livre: " + String.format("%.2f", (cr.tempoSegundosLivre/60d)) + " min"                            
            );
        }
        // x/0 -> N.a.
        taxaOcupacaoCaixasTotal = tempoMinutosCaixasTotalOcupadas/ (tempoMinutosCaixasTotalOcupadas+tempoMinutosCaixasTotalLivres);
        
        System.out.println( "#All"+
                    " Taxa ocupacao: " + String.format("%.2f", taxaOcupacaoCaixasTotal) +
                    " Tempo ocupado: " + String.format("%.2f",(tempoMinutosCaixasTotalOcupadas)) +
                    " min Tempo livre: " + String.format("%.2f",(tempoMinutosCaixasTotalLivres)) + " min"                            
            );
        
//        this.nClientesFinalizados = nClientesFinalizados;
//        this.tempoMedioDeEsperaClientesEmFila = tempoMedioDeEsperaClientesEmFila;
//        this.tempoMedioDeAtendimentoClientesEmCaixa = tempoMedioDeAtendimentoClientesEmCaixa;
//        this.tempoMaximoEsperaFila = tempoMaximoEsperaFila;
//        this.tempoMinutosCaixasTotalOcupadas = tempoMinutosCaixasTotalOcupadas;
//        this.tempoMinutosCaixasTotalLivres = tempoMinutosCaixasTotalLivres;
//        this.taxaOcupacaoCaixasTotal = taxaOcupacaoCaixasTotal;   
        
        return new RelatorioSimulacao(nClients, 
                tempoMedioDeEsperaClientesEmFila, tempoMedioDeEsperaClientesEmCaixa,
                tempoMaximoEsperaFila, 
                tempoMinutosCaixasTotalOcupadas, tempoMinutosCaixasTotalLivres,
                taxaOcupacaoCaixasTotal,
                finalMediaClientesEspera
        );
        
    }
    
    public class ClienteRegisto{
        public boolean finished = false;
        final long tempoEntrada;
        long tempoEntradaCaixa;
        long tempoSaidaCaixa;
        final long idCliente;
        long idCaixa;
        
        public ClienteRegisto(Duration tempoEntradaDuration, long idCliente){
            this.tempoEntrada = tempoEntradaDuration.getSeconds();
            this.idCliente = idCliente;
        }
    }
    
    public class CaixaRegisto{
        public final long id;
        public final long tempoSegundosLivre;
        public final long tempoSegundosOcupada;
        public final double taxaOcupacao;
        
        public CaixaRegisto(long caixaId, Duration tempoLivre, Duration tempoOcupada ){
            id = caixaId;
            tempoSegundosLivre = tempoLivre.getSeconds();
            tempoSegundosOcupada = tempoOcupada.getSeconds();
            
            long tempoTotal = tempoSegundosLivre + tempoSegundosOcupada;
            
            if (tempoTotal <= 0) {
                taxaOcupacao = 0;
            } else {
                taxaOcupacao = ( (double) tempoSegundosOcupada / tempoTotal);
            }
            
        }
    }
    
    public class Ponto{        
        public final long x;
        public final int y;        
        public Ponto (long x, int y){ this.x = x; this.y = y; }
        @Override
        public String toString() { return ""+ x + ";"+y;}        
    }
    public List getPontos(){return this.finalListaGraficoClientesEspera;}
}
