/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.estatistica;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hugo
 */
public class  Registo {
    public static List<String> logEventos = new ArrayList<String>();
    public static List<String> logCriacaoEventos = new ArrayList<String>();
    
    public static void addRegistoCriacaoEvento(String myStr) {logCriacaoEventos.add(myStr);}
    public static void addRegistoOcorrenciaDeEvento(String myStr) {logEventos.add(myStr);}//System.out.println(myStr);}
    
    public static void printAll(){
        for (String s : logCriacaoEventos ) {
            System.out.println(s);
        }
        
        for (String s : logEventos ) {
            System.out.println(s);
        }
    }
    
    public static void clearRegisto(){
        logEventos.clear();
        logCriacaoEventos.clear();
    }
}
