/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.estatistica;

import java.util.Random;
import mosip2019.motor.Estado;

/**
 *
 * @author Hugo
 */
public class GestorDistribuicoes {
    
    
    public static final double TEMPO_MEDIO_ENTRE_CHEGADAS_SEG = 16.43026;
    public static final double TAXA_PRIORITARIOS = 0.11;
    
    public Random geradorRandomsTaxaPrioritarios; 
    public Random geradorRandomsTempoEntreProximoCliente; 
    public Random geradorRandomsTempoAtendimento; 
    
//    private double clienteDesvioPadraoSegundos = 3 * 60 ;
//    private double clienteTempoMedioEntreClientesSegundos = 5 * 60;
//    
//    private double caixaDesvioPadraoSegundos = 3 * 60;
//    private double caixaTempoMedioDeAtendimentoSegundos = 5 * 60;
//    
    
    // +65 anos 21.3%           2017
    // 86.154 nascimentos       em 10.300,300 (2017)  0.836% gravidas
    // 2015 85500 2016 87126 = 172626 em 10.300,300 = 1.67%% menores de 2 anos.
    // 1% populacao nao consegue subir degraus.
    // 0 a 24.8%.
    
    public double clienteTaxaDePrioritarios = TAXA_PRIORITARIOS; //0.05;
    public double clienteTemposMedioEntreChegadasSeg = TEMPO_MEDIO_ENTRE_CHEGADAS_SEG;
    public boolean isHoraPonta = false;
    public double multiplicadorHoraPonta = 1;
        
    
    public GestorDistribuicoes(long mySeed){
        geradorRandomsTaxaPrioritarios = new Random();
        geradorRandomsTempoEntreProximoCliente = new Random();
        geradorRandomsTempoAtendimento = new Random();
        
        geradorRandomsTaxaPrioritarios.setSeed(mySeed); 
        geradorRandomsTempoEntreProximoCliente.setSeed(mySeed * 2); 
        geradorRandomsTempoAtendimento.setSeed(mySeed * 3); 
        isHoraPonta = false;
    }
    
    public boolean setTemposMedioEntreChegadasSeg(double tempo){
        if (Double.isNaN(tempo) || tempo < 0) return false;
        this.clienteTemposMedioEntreChegadasSeg = tempo;
        return true;
    }
    public boolean setTaxaPrioritarios(double taxa){
        if (Double.isNaN(taxa) || taxa < 0) return false;
        this.clienteTaxaDePrioritarios = taxa;
        return true;
    }
    
    public boolean setMultiplicadorHoraPontaAndTurnTrue(double multiplicador){
        if (Double.isNaN(multiplicador)) return false;
        this.multiplicadorHoraPonta = multiplicador;
        this.isHoraPonta = true;
        return true;
    }
    
    
    public long gerarTempoExponencialSegundosEntreProximoCliente(){        
        double media = this.clienteTemposMedioEntreChegadasSeg;          
        return gerarValorDistribuicaoExponencial(media);
    }
    
    public long gerarTempoExponencialSegundosEntreProximoClienteCheckandoHoraDePonta(){        
        if(!isHoraPonta) return gerarTempoExponencialSegundosEntreProximoCliente();
        
        double media = this.clienteTemposMedioEntreChegadasSeg;      
        double multiplicador = this.multiplicadorHoraPonta;
        long tempoAgora = Estado.supermercado.relogio.tempo.getSeconds();
        long tempoFim = Estado.supermercado.relogio.tempoDeFim.getSeconds();        
        
        double mediaCorrigida = calculoDaMediaCorrigidaComOMultiplicador(media, multiplicador, tempoAgora, tempoFim );
        return gerarValorDistribuicaoExponencial(mediaCorrigida);
    }
    
    public double calculoDaMediaCorrigidaComOMultiplicador(double media, double multiplicador, long tempoAgora, long tempoFim ){
        double mediaPico = media * multiplicador;
        
        long tempoMeio = tempoFim /2;
        
        double fracao = (double) tempoAgora/ tempoFim;
        
        double mediaCorrigida = media;
        
        if (fracao <= 0.5){
            
            mediaCorrigida = media + ((fracao * 2) * (mediaPico - media));
        }        
        else{
            mediaCorrigida = mediaPico - (( (fracao - 0.5) * 2) * (mediaPico - media));
        }
        return mediaCorrigida;
    }
    
    
    
    
    
    
    public long gerarValorDistribuicaoExponencial(double media){
        //distribuicao exponencial: F(x) = (1/u) * e^ -(x/u) , onde u e a media.
        // funcao distribuicao comulaiva: 1-e(x/u)
        // inversa da comulativa = 
        double probabilidade = 0;
        probabilidade = geradorRandomsTempoEntreProximoCliente.nextDouble();
        double aux = Math.log(1- probabilidade);
        double tempo = -1 * media * aux;
        return Math.round(tempo);
    }
    
    public long gerarTempoSegundosAtendimentoEmCaixa(){
        double probabilidade = 0;
        probabilidade = geradorRandomsTempoEntreProximoCliente.nextDouble();
        return (long) gerarTempoAtendimentoEmCaixa(probabilidade);
    }
    
    
    
    

//    public void setClienteDesvioPadraoSegundos(double tempo){this.clienteDesvioPadraoSegundos = tempo;}
//    public void setClienteTempoMedioEntreClientesSegundos(double tempo){this.clienteTempoMedioEntreClientesSegundos = tempo;}
//    
//    public long gerarTempoEntreProximoCliente(){
//        double tempoEspera = -1;
//        double tempoMedioEspera = clienteTempoMedioEntreClientesSegundos;
//        double desvioPadrao = clienteDesvioPadraoSegundos;
//        int retries = 0;
//        int max_retries = 1000;
//        
//        while(tempoEspera < 0 && retries < max_retries){
//            tempoEspera = (geradorRandomsTempoEntreProximoCliente.nextGaussian() * desvioPadrao) + tempoMedioEspera;        
//            retries++;
//        }
//        
//        long tempoEsperaLong = Math.round(tempoEspera);
//        if (tempoEsperaLong < 0) tempoEspera = 0;
//        return tempoEsperaLong;
//    }

    
//    public void setCaixaDesvioPadraoSegundos(double tempo){this.caixaDesvioPadraoSegundos = tempo;}
//    public void setCaixaTempoMedioDeAtendimentoSegundos(double tempo){this.caixaTempoMedioDeAtendimentoSegundos = tempo;}
//    
//    public long gerarTempoAtendimentoEmCaixa(){
//        double tempoEspera = -1;
//        double tempoMedioEspera = caixaTempoMedioDeAtendimentoSegundos;
//        double desvioPadrao = caixaDesvioPadraoSegundos;
//        int retries = 0;
//        int max_retries = 1000;
//        
//        while(tempoEspera < 0 && retries < max_retries){
//            tempoEspera = (geradorRandomsTempoAtendimento.nextGaussian() * desvioPadrao) + tempoMedioEspera;        
//            retries++;
//        }
//        
//        long tempoEsperaLong = Math.round(tempoEspera);
//        if (tempoEsperaLong < 0) tempoEspera = 0;
//        return tempoEsperaLong;
//    }
   
    
    
    
    
    public boolean isClientePrioritario(){
        double p = geradorRandomsTaxaPrioritarios.nextDouble();
        return (p < this.clienteTaxaDePrioritarios);
    }
    
    
    
    public double vetorCaixas[] = {
        15.776,  26.101,  27.154,  32.307,  33.846,  34.792,  45.734,  51.801,  52.444,  53.131,  53.977,
        57.361,  64.148,  69.563,  69.986,  82.652,  91.698, 104.643, 116.985, 176.492
    };
    
    public double gerarTempoAtendimentoEmCaixa(double p) {
        
        //double pPrimeiro = (1.0d/ (vetorCaixas.length-1)) * vetorCaixas[0];
        double valorMinimo = vetorCaixas[0];
        double valorMaximo = vetorCaixas[vetorCaixas.length-1];
        
        //Validacoes
        if (p <= 0) return valorMinimo;
        if (p >= 1) return valorMaximo;
        
        int indice2 = (int)(p * 19)+1;
        int indice1 = indice2 - 1;
        
        double probIndice2 = indice2 / 19d;
        double probIndice1 = indice1 / 19d;
        
        double tempoIndice2 = vetorCaixas[indice2];
        double tempoIndice1 = vetorCaixas[indice1];
        
        
        // Interpolacao:
        //declive da reta:
        double declive = (tempoIndice2 - tempoIndice1) / (probIndice2 - probIndice1);
        
        double tempo = declive * (p - probIndice1) +tempoIndice1;
        
        return tempo;
    }
    
    
    /**
     * Imprime 100 tempos de atendimentos em caixa. Serve para imprimir um grafico da funcao de probabilidade
     */
    public void printFuncaoCaixas(){
        
        int max = 100;
        printP(0, gerarTempoAtendimentoEmCaixa(0));
        for(int i = 0; i < max; i++){
            double p =  i/ ((double) max) ;
            double t = gerarTempoAtendimentoEmCaixa(p);
            printP(p,t);
        }
    }
    
    public void printP(double p, double t){
        System.out.println("" + String.format("%.2f",p) + ";" + String.format("%.0f",t));
    }
    
    
    
    
    
    
    
    
    
    
    
}
