/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mosip2019.estatistica;

/**
 *
 * @author Hugo
 */
public class RelatorioSimulacao {
    
    
    
    public final int nClientesFinalizados;
    public final double tempoMedioDeEsperaClientesEmFila ;
    public final double tempoMedioDeAtendimentoClientesEmCaixa ;
    public final double tempoMaximoEsperaFila ;
    
    public final double tempoMinutosCaixasTotalOcupadas ;
    public final double tempoMinutosCaixasTotalLivres ;
    public final double taxaOcupacaoCaixasTotal ;
    
    public final double mediaClientesEspera;
    
    public RelatorioSimulacao(
            int nClientesFinalizados,
            double tempoMedioDeEsperaClientesEmFila,
            double tempoMedioDeAtendimentoClientesEmCaixa,
            double tempoMaximoEsperaFila,            
            double tempoMinutosCaixasTotalOcupadas,
            double tempoMinutosCaixasTotalLivres,
            double taxaOcupacaoCaixasTotal,
            double mediaClientesEspera
    ){
        this.nClientesFinalizados = nClientesFinalizados;
        this.tempoMedioDeEsperaClientesEmFila = tempoMedioDeEsperaClientesEmFila;
        this.tempoMedioDeAtendimentoClientesEmCaixa = tempoMedioDeAtendimentoClientesEmCaixa;
        this.tempoMaximoEsperaFila = tempoMaximoEsperaFila;
        this.tempoMinutosCaixasTotalOcupadas = tempoMinutosCaixasTotalOcupadas;
        this.tempoMinutosCaixasTotalLivres = tempoMinutosCaixasTotalLivres;
        this.taxaOcupacaoCaixasTotal = taxaOcupacaoCaixasTotal;        
        this.mediaClientesEspera = mediaClientesEspera;
    }

    @Override
    public String toString() {
        return "" + this.nClientesFinalizados
                + ";" + String.format("%.2f", this.tempoMedioDeEsperaClientesEmFila)
                + ";" + String.format("%.2f", this.tempoMedioDeAtendimentoClientesEmCaixa)
                + ";" + String.format("%.2f", this.tempoMaximoEsperaFila)
                + ";" + String.format("%.2f", this.tempoMinutosCaixasTotalOcupadas)
                + ";" + String.format("%.2f", this.tempoMinutosCaixasTotalLivres)              
                + ";" + String.format("%.2f", this.taxaOcupacaoCaixasTotal)
                + ";" + String.format("%.2f", this.mediaClientesEspera)
                ;
    }
    
    public static String getHeader(){
        return "" + "nClientes"
                + ";" + "tempoMedioDeEsperaClientesEmFila"
                + ";" + "tempoMedioDeAtendimentoClientesEmCaixa"
                + ";" + "tempoMaximoEsperaFila"
                + ";" + "tempoMinutosCaixasTotalOcupadas"
                + ";" + "tempoMinutosCaixasTotalLivres"              
                + ";" + "taxaOcupacaoCaixasTotal"
                + ";" + "mediaClientesEspera"
                ;
    }
    
    
    
}
